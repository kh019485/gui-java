package Drones;

import javafx.scene.paint.Color;

public class Obstacle extends Drone {
	
	 /** 
     * adjusts obstacle ball/drone has no function
     */
    protected void adjustDrone() {
        // nothing to do
    }
    
    protected String getStrType() {
        return "Obstacle";
    }

   
    protected void checkDrone(Arena a) {
        // nothing to do
    }

    /**
     * draws the obstacle of those attributes in the specified colour
     * @param Obstaclex
     * @param Obstacley
     * @param Obstacler
     */
    public Obstacle(double Obstaclex, double Obstacley, double Obstacler) {
        super(Obstaclex, Obstacley, Obstacler);
        colour = "GREY";
    }
    
    /**
     * creates and draws obstacles 
     * @param mc
     */
    public void drawDrone(ObjectCanvas mc) {
        mc.drawRect(x, y, radian, colour);
    }
    
   

    
}
