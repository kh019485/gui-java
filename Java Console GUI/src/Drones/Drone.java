package Drones;


import javafx.scene.paint.Color;
import java.io.Serializable;



public abstract class Drone implements Serializable {
	double x, y, radian;
	int DroneID;												
    String colour;								
    static int counter = 0;

    Drone() {
        this(100, 100, 10);
    }
    /**
     * construct a drone/ball of radian Ballsr at Ballsx,Ballsy of this colour
     * @param Dronex
     * @param Droney
     * @param Droner
     */
    Drone(double Dronex, double Droney, double Droner) {
        x = Dronex;
        y = Droney;
        radian = Droner;
        DroneID = counter++;			
        colour = "Red";
    }
   
    public double getX() { return x; }
   
    public double getY() { return y; }
 
    public double getRad() { return radian; }
 
    public void setXY(double nx, double ny) {
        x = nx;
        y = ny;
    }
  
    public int getID() {return DroneID; }
  
    public void drawDrone(ObjectCanvas mc) {
        mc.drawCircle(x, y, radian, colour);
    }
   
   
    public String toString() {
        return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
    }
    
    public boolean hit(double ox, double oy, double or) {
        return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+radian)*(or+radian);
    }	
    
    protected String getStrType() {
        return "Ball";
    }
   
    protected abstract void adjustDrone();
    /**
     * checks if drone is in arena 
     * @param a
     */
    protected abstract void checkDrone(Arena a);
  
   
  	

  
    public boolean hitting (Drone oDrone) {
        return hit(oDrone.getX(), oDrone.getY(), oDrone.getRad());
    }
}