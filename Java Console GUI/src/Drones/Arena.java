package Drones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * public c;lass to create arena for drones 
 */
public class Arena implements Serializable {
    double ArenaSizeX;
    double ArenaSizeY;						
    private ArrayList<Drone> allDrones; 
    
    
    /**
     * create arena and add objects 
     * @param xS
     * @param Sizey
     */
    Arena(double Sizex, double Sizey){
        ArenaSizeX = Sizex;
        ArenaSizeY = Sizey;
        allDrones = new ArrayList<Drone>();					//intialize arraylist to be empty 
        allDrones.add(new Balls(Sizex/2, Sizey*0.8, 10, 45, 6));	// adds small black ball
        allDrones.add(new Opponents(Sizex/2, Sizey/5, 35));			// adds enemy drone
        allDrones.add(new Character(Sizex/2, Sizey-20, 20));		// adds Character drone
        allDrones.add(new Obstacle(Sizex/20, 1*Sizey/5, 25)); // adds blocking objects/walls
        allDrones.add(new Obstacle(Sizex/8, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(Sizex/4, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(Sizex/3, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(Sizex, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(2*Sizex/3, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(2*Sizex/8, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(Sizex-100, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(Sizex-70, 1*Sizey/5, 25));
        allDrones.add(new Obstacle(Sizex-40, 1*Sizey/5, 25));
    }
    
    // sets size of arena 
    Arena() {
        this(500, 400);			
    }
    
    //Getter and Setter Functions 
    public double getX() {
        return ArenaSizeX;
    }
    
    public double getY() {
        return ArenaSizeY;
    }
    
   
    /**
     * check all balls/drones, change angle of moving balls if needed
     */
    public void checkDrones() {
        for (Drone b : allDrones) b.checkDrone(this);	
    }
    
    public void adjustDrones() {
        for (Drone b : allDrones) b.adjustDrone();
    }
    
    public void drawArena(ObjectCanvas mc) {
        for (Drone b : allDrones) b.drawDrone(mc);	//Function to draw arena parts 
    }
    
   
    /**
     * checks the angle of the drone and its hit conditions depending on a wall or other drone/ball
     * @param x				ball x position
     * @param y				y
     * @param rad			radius
     * @param ang			current angle
     * @param notID			don't check id of the drone/ball
     * @return				new angle
     */
    public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
        double ans = ang;
        if (x < rad || x > ArenaSizeX - rad) ans = 180 - ans;

        if (y < rad || y > ArenaSizeY - rad) ans = - ans;


        for (Drone b : allDrones)
            if (b.getID() != notID && b.hit(x, y, rad)) ans = 180*Math.atan2(y-b.getY(), x-b.getX())/Math.PI;

        return ans;	
    }
    
    /**
     * creates and sets x and y  coordinates for Character 
     * @param x
     * @param y
     */
    public void setPaddle(double x, double y) {
        for (Drone b : allDrones)
            if (b instanceof Character) b.setXY(x, y);
    }

    /**
     * function to check if opponent has hit a drone 
     * @param opponent drone 
     * @return 	if hit or not 
     */
    public boolean checkHit(Drone target) {
        boolean ans = false;
        for (Drone b : allDrones)
            if (b instanceof Balls && b.hitting(target)) ans = true;
        // try all balls
        return ans;
    }
    
    
    /**
     * return list of strings defining each ball/drone
     * @return
     */
    public ArrayList<String> describeAll() {
        ArrayList<String> ans = new ArrayList<String>();		// set up empty arraylist
        for (Drone b : allDrones) ans.add(b.toString());			// add string defining each ball
        return ans;												// return string list
    }
    public boolean inBorder(Drone target) {
    	return !(target.getX() - target.getRad() < 0 || target.getY() - target.getRad() < 0 || target.getX() + target.getRad() > ArenaSizeX || target.getY() + target.getRad() > ArenaSizeY);
    }
    /**
     *adds drone to certain coordinates 
     * 
     */
    public void addDrone() {
        allDrones.add(new Balls(ArenaSizeX/2, ArenaSizeY*0.8, 10, 60, 5));
    }
}
