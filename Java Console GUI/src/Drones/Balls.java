package Drones;

import javafx.scene.paint.Color;

/**
 * class to create drone/ball objects
 */
public class Balls extends Drone {

    double Degree, Speed;			// Degree and speed of travel
    
    
    public Balls(double Ballsx, double Ballsy, double Ballsr, double Ballsa, double Ballss) {
        super(Ballsx, Ballsy, Ballsr);
        Speed = Ballss;
        Degree = Ballsa;
        colour = "BLUE";
    }
    
   

    /**
     * change Degree of the drone 
     * @param a   ballArena
     */
    @Override
    protected void checkDrone(Arena a) {
        Degree = a.CheckDroneAngle(x, y, radian, Degree, DroneID);
    }

    /**
     * change drone depending on speed travelling 
     */
    @Override
    protected void adjustDrone() {
        double radDegree = Degree*Math.PI/180;		
        x += Speed * Math.cos(radDegree);		
        y += Speed * Math.sin(radDegree);		
    }
    /**
     * return and define drone 
     */
    protected String getStrType() {
        return "Drone";
    }

    
    
    /**
     * draw a circle onto the canvas at point x,y
     * @param mc
     */
    public void drawDrone(ObjectCanvas mc) {
        mc.drawCircle(x, y, radian, colour);
    }
}

